#
# build/add_command.sh - UNIX build script (add command)
#

#
# This script contains the functions shared by several of the commands and/or the main
# build script.
#
#---
# PACKAGE functions
#
# * check_package   Checks if a package with the name given exists and whether it's in
#                   the given path.
#
#---
# MISC functions
#
# * display_help    Display the help information on the selected command or general
#                   information is no command is given.
#
#---
# CARGO functions
#
# * cargo_add_to_workspace  Add a package to the workspace members.
#
#---
# GITLAB functions
#
# * gitlab_check_project    Check if a project exists and whether it's in the given
#                           subgroup.
# * gitlab_create_project   Create a public project on Gitlab.
#

#----------------------------------------------------------------------------------------
# PACKAGE functions
#------------------

#---
# Check Package
#
# Usage:
#   check_package <PATH> <PACKAGE>
#
# Returns:
#   0   Package doesn't exist
#   1   An error occured calling the function
#   2   Package exists in the path given
#   3   Package exists in another path
#---
function check_package
{
    if [ $# -ne 2 ]
    then
        echo "[Error] The check package name requires the 'path' and 'package'"
        return 1
    fi

    typeset package=$2
    typeset path="$1/$2"

    typeset tmp=$(find . -name $package -type d | grep -v "git" | grep -v "target")
    if [ -z $tmp ]
    then
        return 0
    fi

    if [ -d $path ]
    then
        return 2
    fi

    return 3
}

#----------------------------------------------------------------------------------------
# MISC functions
#---------------

#---
# Display help
#
# Usage:
#   display_help
#
# Exits with success status
#---
function display_help
{
    typeset info_file="$tools_build_path/docs/general.txt"

    case $cmd in

        $NO_COMMAND)
            if [ $ci -eq 1 ]
            then
                echo "build.sh (continuous integration)"
                info_file="$tools_build_path/docs/ci.txt"
            else
                echo "build.sh (general)"
                info_file="$tools_build_path/docs/general.txt"
            fi
            ;;

        $CMD_ADD)
            echo "build.sh (add command)"
            info_file="$tools_build_path/docs/add_command.txt"
            ;;

        $CMD_BUILD)
            echo "build.sh (build command)"
            info_file="$tools_build_path/docs/build_command.txt"
            ;;

        $CMD_DOC)
            echo "build.sh (doc command)"
            info_file="$tools_build_path/docs/doc_command.txt"
            ;;
            
        $CMD_RUN)
            echo "build.sh (run command)"
            info_file="$tools_build_path/docs/run_command.txt"
            ;;

        $CMD_TEST)
            echo "build.sh (test command)"
            info_file="$tools_build_path/docs/test_command.txt"
            ;;
    esac

    echo
    cat $info_file
    echo

    exit 0
}

#----------------------------------------------------------------------------------------
# CARGO functions
#----------------

#---
# Add a member to a Cargo workspace
#
# Usage:
#   cargo_add_to_workspace <PATH> <PACKAGE>
#
# Returns:
#   0   The project is added
#   1   An error occured adding the member
#---
function cargo_add_to_workspace
{
    if [[ $# -ne 2 ]]
    then
        return 1
    fi

    typeset new_path=$1
    typeset new_package=$2
    typeset new_member="$new_path/$new_package"
    typeset package="tundra-wolf"

    typeset tmp_cargo="$tmp_path/Cargo.toml"
    typeset tmp_cargo_packages="$tmp_path/Cargo.packages"

    rm -rf $tmp_cargo_packages $tmp_cargo
    touch $tmp_cargo_packages

    for package in "${!packages[@]}"
    do
        echo "${packages[$package]}" >> $tmp_cargo_packages
    done
    echo "$new_member" >> $tmp_cargo_packages

    cat $tmp_cargo_packages | sort > $tmp_cargo_packages.sorted

    echo "[workspace]" > $tmp_cargo
    echo "members = [" >> $tmp_cargo

    for member in $(cat $tmp_cargo_packages.sorted)
    do
        echo "    \"$member\"," >> $tmp_cargo
    done

    echo "]" >> $tmp_cargo
    echo "resolver = \"2\"" >> $tmp_cargo
    echo >> $tmp_cargo

    cp $tmp_cargo $script_path/Cargo.toml

    rm -rf $tmp_cargo_packages $tmp_cargo_packages.sorted $tmp_cargo

    return 0
}

#----------------------------------------------------------------------------------------
# GITLAB functions
#-----------------

#---
# Check project on Gitlab
#
# Usage:
#   gitlab_check_project <SUBGROUOP> <PROJECT>
#
# Returns:
#   0   Project doesn't exist
#   1   An error occured calling the function
#   2   Project exists in the subgroup given
#   3   Project exists in another subgroup
#---
function gitlab_check_project
{
    if [ $# -ne 2 ]
    then
        echo "[Error] The check project requires the 'project' and 'subgroup'"
        return 1
    fi

    typeset project=$2
    typeset subgroup="$1/$2"

    return 1
}

#---
# Create a new private project on Gitlab
#
# Usage:
#   gitlab_create_project <SUBGROUP> <PROJECT>
#
# Returns:
#   0   Project created successfully
#   1   An error occured creating the project
#---
function gitlab_create_project
{
    if [ $# -ne 2 ]
    then
        echo "[Error] The check project requires the 'project' and 'subgroup'"
        return 1
    fi

    typeset project=$2
    typeset subgroup="$1"

    # Create remote project
    git remote add  -t main origin "git@gitlab.com:$subgroup/$project.git"
    if [[ $? -ne 0 ]]
    then
        return 1
    fi

    # Push changes to the remote repository
    git push --set-upstream origin master
    if [[ $? -ne 0 ]]
    then
        return 1
    fi

    echo "[Warning]"
    echo "    Please make sure the project is publicly accessible on Gitlab? Currently"
    echo "    we have no way to do this programmatically."
    echo "[Warning]"

    return 0
}
